# projects
* support registration of custom things, e.g.:
  * add `alias cd-toolbelt-meta` to `.bash_aliases`
  * copy tmuxinator conf

## general setup
* `pyenv virtualenv 3.7.9 $(cat .python-version)`

## alto_utils
* `pip install pipenv`
* `pipenv sync --dev`

## eet
* comment alto git deps in requirements-dev.txt
* before `pip install -r requirements-dev.txt` run:
  `apt-get install libxml2-dev libxmlsec1-dev libxmlsec1-openssl`
  * these are requirements of `xmlsec`
