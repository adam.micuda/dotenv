# example

# ensure that specified Python is installed
pyenv virtualenv 3.7.5 alto_django_utils3.7.5
pyenv local alto_django_utils3.7.5
pip install pip==18.1 # see requirements{-dev}
# copy (or create) `pytest_watch.ini`

# example: update dev requirements
rm requirements-dev.txt
pip-compile requirements-dev.in
pip install -r requirements-dev.txt
