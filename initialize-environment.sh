#!/usr/bin/env bash
python_version="${PYTHON_VERSION:-3.9.6}"
node_version="${NODE_VERSION:-12}"

local_bin="$HOME/.local/bin"
mkdir -p "$local_bin"

# general settings
## TODO: do not display askerisks while typing password
sudo mv /etc/sudoers.d/0pwfeedback{,.disabled}

apt update

# TODO: change package repositories source to local mirror

# install common utils
sudo apt install -y ripgrep fd-find htop tree ranger jq xclip pwgen sshfs chromium-browser apt-file

apt-file update

# ripgrep
mkdir $HOME/.config
cp -r $DOTENV_SOURCE/home/.config/ripgrep $HOME/.config

sudo apt install -y git
cp $DOTENV_SOURCE/home/.gitconfig $HOME/

## create symbolic link `fd-find` to `fd` instead of alias
ln -s /usr/bin/fdfind ~/.local/bin/fd

## install exa
### exa is in official Ubuntu repository from 20.10
wget -O /tmp/exa.zip https://github.com/ogham/exa/releases/download/v0.9.0/exa-linux-x86_64-0.9.0.zip
unzip /tmp/exa.zip -d $local_bin/
mv $local_bin/{exa-linux-x86_64,exa}

## install bat
### in official Ubuntu repository is older version
$(cd /tmp/ && wget https://github.com/sharkdp/bat/releases/download/v0.16.0/bat_0.16.0_amd64.deb)
sudo dpkg -i /tmp/bat_0.16.0_amd64.deb

## install grc
wget -O /tmp/grc.tar.gz https://github.com/garabik/grc/archive/v1.11.3.tar.gz
mkdir /tmp/grc
tar xf /tmp/grc.tar.gz -C /tmp/grc/
# maybe make own installation script
pushd /tmp/grc/*/
sudo ./install.sh
popd

## install tig
sudo apt install -y build-essential libncurses5-dev
local="$HOME/.local"
wget -O /tmp/tig.tar.gz https://github.com/jonas/tig/releases/download/tig-2.5.4/tig-2.5.4.tar.gz
mkdir /tmp/tig/
tar xf /tmp/tig.tar.gz -C /tmp/tig/
pushd /tmp/tig/*/
make prefix=$local
sudo make install prefix=$local
popd

# TODO: download Franz https://meetfranz.com/download?platform=linux&type=deb

# install Powerline
sudo apt install -y python3-pip python3-setuptools python3-wheel
pip3 install --user powerline-status
## install fonts
mkdir -p $HOME/.local/share/fonts/
wget -O $HOME/.local/share/fonts/PowerlineSymbols.otf https://github.com/powerline/powerline/raw/develop/font/PowerlineSymbols.otf
mkdir -p $HOME/.config/fontconfig/conf.d/
wget -O $HOME/.config/fontconfig/conf.d/10-powerline-symbols.conf https://github.com/powerline/powerline/raw/develop/font/10-powerline-symbols.conf
fc-cache -vf $HOME/.local/share/fonts/

# install pyenv
# see https://github.com/pyenv/pyenv-installer
# https://github.com/pyenv/pyenv/wiki/Common-build-problems # get and install deps programmatically
sudo apt install -y make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev python-openssl
curl https://pyenv.run | bash
# update pyenv
export PYENV_ROOT="$HOME/.pyenv" && export PATH="$PYENV_ROOT/bin:$PATH" && eval "$(pyenv init -)" && eval "$(pyenv init --path)" && eval "$(pyenv virtualenv-init -)"
pyenv update

# install pyenv plugins
# pyenv-default-packages plugin
git clone https://github.com/jawshooah/pyenv-default-packages.git ~/.pyenv/plugins/pyenv-default-packages
cp $DOTENV_SOURCE/home/.pyenv/default-packages $HOME/.pyenv/

# install specified version of Python and set it as global
pyenv install "$python_version"
pyenv global "$python_version"
pyenv shell "$python_version"

# add some Python customization, e.g. set `ic` as global function
cp -r $DOTENV_SOURCE/home/.config/python $HOME/.config/
# TODO: create symbolic link for all Python versions that should be customized
# ln -s ~/.config/python/init.py ~/.local/lib/python3.8/site-packages/usercustomize.py

# install nvm
# TODO: see https://github.com/nvm-sh/nvm
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
# install node (required by YouCompleteMe compilation)
function with-nvm {
	init_nvm='export NVM_DIR="$HOME/.nvm"; \
		[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"'
	echo "$init_nvm && $@" | exec bash
}

with-nvm nvm install $node_version

# tldr
with-nvm npm install -g tldr

# install Poetry
curl -sSL https://install.python-poetry.org | python3 -

# NVIM
## TODO: check if is valid: must be ran in new shell
## TODO: install nvim for root too
## installs `fzf` as a plugin
temp_nvim_binary="/tmp/nvim"
nvim_binary="/usr/local/bin/nvim"
wget https://github.com/neovim/neovim/releases/latest/download/nvim.appimage -O "$temp_nvim_binary"
sudo install --owner=root --group=root --mode=755 "$temp_nvim_binary" "$nvim_binary"
ln -s "$nvim_binary" "/usr/local/bin/vim"

## create neccessary structure
mkdir -p $HOME/.local/share/nvim/{bundle,undodir}
# [ -d "$HOME/.config" ] || mkdir $HOME/.config
NVIM_CONFIG_DIR="$HOME/.config/nvim"
mkdir -p "$NVIM_CONFIG_DIR"

## configuration
# TODO: replace following placeholders in init.vim
# PYTHON3_HOST_PROG=$(pyenv which python)
# NVIM_SNIPPERS_DIR=${$NVIM_SNIPPERS_DIR:-"$NVIM_CONFIG_DIR/snippets"}
cp $DOTENV_SOURCE/home/.config/nvim/* "$NVIM_CONFIG_DIR/"
sudo cp $DOTENV_SOURCE/etc/vim/vimrc.local /etc/vim/

# setup virtualenv
nvim_virtualenv=${NVIM_VIRTUALENV:-"nvim$python_version"}
pyenv virtualenv --system-site-packages "$python_version" "$nvim_virtualenv"
pyenv activate "$nvim_virtualenv"
pip install pynvim
pyenv deactivate

## install prerequisites
with-nvm npm install -g typescript dockerfile-language-server-nodejs bash-language-server
# TODO: do not use `all`, specify extras instead
pip3 install --user mypy pylint pylint-django python-language-server[all] pyls-mypy

## install plugin manager
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
	    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
# TODO: see https://github.com/junegunn/vim-plug

# FIXME: wait for installation of Coc stuff and then quit
# nvim -c ":PlugInstall" -c ":CocInstall coc-pyright coc-tsserver coc-json" -c "qa!"

### setup YouCompleteMe
# TODO: compile YouCompleteMe, for more info see https://github.com/Valloric/YouCompleteMe#installation
sudo apt install -y build-essential cmake python3-dev
# go to YCM dir (path below) and run: git submodule update --init --recursive
with-nvm /usr/bin/python3 $HOME/.local/share/nvim/bundle/YouCompleteMe/install.py --ts-completer


# TMUX setup
sudo apt install -y tmux
## install tmux plugin manager and install plugins
git clone https://github.com/tmux-plugins/tpm $HOME/.tmux/plugins/tpm

## configuration
cp $DOTENV_SOURCE/home/.tmux.conf $HOME/

## install plugins
if [ -n "$TMUX" ]; then
	tmux source $HOME/.tmux.conf
	$HOME/.tmux/plugins/tpm/bindings/install_plugins
else
	tmux -c "tmux source $HOME/.tmux.conf; \
	       	$HOME/.tmux/plugins/tpm/bindings/install_plugins"
fi


### install tmuxinator
sudo apt install -y ruby
gem install --user-install tmuxinator

### configuration
cp $DOTENV_SOURCE/home/.tmuxinator.bash $HOME/

### projects configuration
# TODO: get TMUXINATOR_CONFIG value from cookiecutter template variable
TMUXINATOR_CONFIG=$HOME/.tmuxinator
mkdir -p $TMUXINATOR_CONFIG
cp $DOTENV_SOURCE/home/.tmuxinator/* $TMUXINATOR_CONFIG/

### register `tmuxinator` binary
$(cd $HOME/.local/bin && ln -s $HOME/.gem/ruby/2.7.0/bin/tmuxinator .)  # FIXME: proc mam 2.5.0 kdyz verze ruby je 2.5.1

# Increase maximum of inotify user watchers
SYSCTL_CONF_DIR=/etc/sysctl.d/
[ -d "/etc/sysctl.d" ] || sudo mkdir $SYSCTL_CONF_DIR

MAX_USER_WATCHES=$((3 * `cat /proc/sys/fs/inotify/max_user_watches`))
# TODO: set only if current value is lower than `$MAX_USER_WATCHES`
# TODO: set max_user_watches in `98-custom-settings.conf`
sudo cp $DOTENV_SOURCE/etc/sysctl.d/98-custom-settings.conf $SYSCTL_CONF_DIR
sysctl -p # TODO: try if will not work for this session run:
# sudo sysctl fs.inotify.max_user_watches=$MAX_USER_WATCHES

TODO:
# bash settings
cp $DOTENV_SOURCE/home/.bash_aliases $HOME/
cp $DOTENV_SOURCE/home/.bash_profile $HOME/
cp $DOTENV_SOURCE/home/.bashrc $HOME/

# .bashrc and related must be copied as the last, because the libs are used there

# redis
sudo apt install -y redis-server
sudo systemctl enable redis-server.service
# history can be copied too

# postgres
sudo apt install -y postgresql postgresql-doc

# TODO: use current Postgres version in config path
# TODO: change local connection  method for all users from `peer` to `md5`
# line 90, e.g. sudo vim /etc/postgresql/12/main/pg_hba.conf -c "gg/peer<cr>nncwmd5<cr>"

# TODO: download config, eg. cp -r var/lib/postgresql/ $HOME/postgresql
# TODO: /etc/postgresql/12/main/postgresql.conf:40: data_directory = '/home/postgres/var/lib/postgresql/12/main'
sudo rsync -av /var/lib/postgresql /home/postgres/var/lib/

# nginx
sudo apt install -y nginx


# ADDITIONAL SETUP
# required by pgcli
sudo apt install libpq-dev
