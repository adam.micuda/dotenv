import builtins

try:
  from icecream import ic
except ImportError:
  import pprint
  ic = pprint

builtins.ic = ic

try:
  import pytest
except ImportError:
  tdd_current = lambda func: func
else:
  tdd_current = pytest.mark.tdd_current

builtins.tdd_current = tdd_current
