call plug#begin('~/.local/share/nvim/bundle')

" editing
Plug 'AndrewRadev/switch.vim'
Plug 'christoomey/vim-sort-motion'
Plug 'easymotion/vim-easymotion'
Plug 'honza/vim-snippets'
Plug 'junegunn/vim-easy-align'
Plug 'kana/vim-textobj-entire'
Plug 'kana/vim-textobj-function'
Plug 'kana/vim-textobj-indent'
Plug 'kana/vim-textobj-user'
" Plug 'SirVer/ultisnips'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-surround'
Plug 'wellle/targets.vim'

" display goodies
Plug 'airblade/vim-gitgutter'
Plug 'folke/todo-comments.nvim'
Plug 'folke/trouble.nvim'
Plug 'frazrepo/vim-rainbow'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'kshenoy/vim-signature'
Plug 'majutsushi/tagbar'
Plug 'machakann/vim-highlightedyank'
" Plug 'ryanoasis/vim-devicons'
Plug 'itchyny/lightline.vim'
Plug 'haya14busa/incsearch.vim'
Plug 'haya14busa/incsearch-fuzzy.vim'
Plug 'pgdouyon/vim-evanesco'


" language & framework support
Plug 'editorconfig/editorconfig-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'rust-lang/rust.vim'
Plug 'sheerun/vim-polyglot'
Plug 'Valloric/YouCompleteMe'
Plug 'iamcco/markdown-preview.vim'

" files, windows, buffers, commands
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --key-bindings --completion --no-update-rc' }
Plug 'junegunn/fzf.vim'
Plug 'ryvnf/readline.vim'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-eunuch'

" external integration
Plug 'christoomey/vim-tmux-navigator'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
Plug 'mileszs/ack.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-dispatch'

" colorschemes
Plug 'https://gitlab.com/micuda-toolbelt/vim-theme.git'

call plug#end()
