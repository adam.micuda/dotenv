function! StripTrailingWhitespaces()
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  %s/\s\+$//e
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction

function! CheckBackSpace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

function! ShowDocumentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

function! LightLineFilenameWithModified()
  let filename = expand('%:t') ==# '' ? '[No Name]' : expand('%:t')
  let modified = &modified ? ' +' : ''

  return filename . modified
endfunction

function! LightLineCocWarnings()
  let warning_sign = get(g:, 'coc_status_warning_sign', 'W')
  let coc_info     = get(b:, 'coc_diagnostic_info', {})

  return warning_sign . get(coc_info, 'warning', 0)
endfunction

function! LightLineCocErrors()
  let error_sign = get(g:, 'coc_status_error_sign', 'E')
  let coc_info   = get(b:, 'coc_diagnostic_info', {})

  return error_sign . get(coc_info, 'error', 0)
endfunction

function! LightLinePythonInfo()
  " only for python files
  if &filetype !=# 'python'
    return ''
  endif

  " show name of virtualenv
  let env_name = $VIRTUAL_ENV
  if env_name !=# ''
    return split(env_name, '/')[-1]
  endif

  " ... or version of python if there is no virtualenv active
  if has('python3')
    python3 import sys, vim
    python3 major, minor, micro = sys.version_info[:3]
    python3 vim.command(f"let g:python_version = '{major}.{minor}.{micro}'")
  elseif has('python')
    python import sys, vim
    python major, minor, micro = sys.version_info[:3]
    python vim.command("let g:python_version = '{major}.{minor}.{micro}'".format(major = major, minor = minor, micro = micro))
  endif

  return 'Python ' . get(g:, 'python_version', 'unknown')
endfunction
