" maximize window after entering
autocmd WinEnter * wincmd _
" autocmd WinEnter * wincmd |

" filetype associations
autocmd BufRead,BufNewFile *.txt         set filetype=zim
autocmd BufRead,BufNewFile *.md          set filetype=markdown
autocmd BufRead,BufNewFile *.asd         set filetype=lisp
autocmd BufRead,BufNewFile *.tex         set filetype=tex
autocmd BufRead,BufNewFile *.scheme      set filetype=scheme
autocmd BufRead,BufNewFile *.god         set filetype=ruby
autocmd BufRead,BufNewFile *.em          set filetype=emblem
autocmd BufRead,BufNewFile *.py          set textwidth=100 tabstop=2 softtabstop=2 shiftwidth=2 expandtab foldmethod=indent
autocmd BufRead,BufNewFile */foodie/*.py set textwidth=100 tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab foldmethod=indent
" autocmd FileType sh set noexpandtab

autocmd BufWritePre * :call StripTrailingWhitespaces()

" use enter in normal mode to insert newline
autocmd CmdwinEnter * nnoremap <CR> <CR>
autocmd BufReadPost quickfix nnoremap <CR> <CR>

" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

autocmd User CocStatusChange,CocDiagnosticChange call lightline#update()

" automatically rebalance windows on vim resize
autocmd VimResized * :wincmd =
