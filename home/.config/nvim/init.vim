set rtp+=~/.fzf

runtime plugins.vim

colorscheme vim_theme

runtime plugin_settings.vim
runtime base.vim
runtime functions.vim
runtime mappings.vim
runtime autocmds.vim
runtime highlights.vim
