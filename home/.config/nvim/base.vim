" needed for % to jump begin/def/class <-> end, etc.
runtime macros/matchit.vim

set hlsearch
set incsearch
set ignorecase
set smartcase

set encoding=utf-8
set number
set relativenumber
set backupcopy=yes
set noshowmode
set updatetime=100
set background=dark

set winheight=5
set winminheight=5
set winwidth=85
set winminwidth=65

set laststatus=2
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
set textwidth=100

" enable mouse
" set mouse=a

" use ripgrep as grep program
set grepprg=rg

" always show at least 5 lines around cursor
set scrolloff=3
set sidescrolloff=3
set display+=lastline

" share clipboard with os
set clipboard=unnamed

" always show signcolumns
set signcolumn=yes

" highlight current line
set cursorline

" visual symbols for nonprintable characters
set listchars=tab:▸\ ,eol:¬

" see :h fo-table, add a to reformat paragraph after every change
" (:set formatoptions+=a)
set formatoptions=tcrqj
" set formatprg=par\ -w80r

" maintain undo btw sessions
set undodir=~/.local/share/nvim/undodir
set undofile

" code folding options
set nofoldenable
set foldmethod=syntax
set foldopen-=block
set foldlevelstart=3

" open new splits right and below
set splitbelow
set splitright

" complete with tab, only to longest match
" imap <tab> <c-p>
set completeopt=menu,longest,preview

" completion in command
set wildmode=longest,list:longest
set wildignore+=.git*,coverage/**,tmp/**,**~

" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" for compatibility with fish shell
set shell=/bin/bash

" if hidden is not set, TextEdit might fail.
set hidden

" don't give |ins-completion-menu| messages.
set shortmess+=c

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}
