" Coc settings
hi CocErrorSign ctermfg=Red  guifg=#ff0000
hi CocInfoSign  ctermfg=Blue guifg=#15aabf
hi CocHintSign  ctermfg=Blue guifg=#15aabf

" set search matches highlighting color
hi Search ctermbg=13
