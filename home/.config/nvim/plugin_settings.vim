let g:loaded_python_provider = 0 " disable python2

let g:ackprg = 'rg --vimgrep --smartcase'

" TODO: let g:python3_host_prog = "$PYTHON3_HOST_PROG"
let g:python3_host_prog = "/home/jcas/.pyenv/versions/nvim3.9.6/bin/python"

let g:highlightedyank_highlight_duration=500

" disable tmux navigator when zooming the Vim pane
let g:tmux_navigator_disable_when_zoomed = 1

"  make all sorts case insensitive and remove duplicates
let g:sort_motion_flags = "ui"

let g:NERDTreeIgnore = ['\.bst$','\.dia$','\.eps$','\.pdf$','\~$','\.pyc$','^__pycache__$']

let g:UltiSnipsExpandTrigger="<c-space>"
let g:UltiSnipsJumpForwardTrigger="<M-j>"
let g:UltiSnipsJumpBackwardTrigger="<M-k>"
" TODO: let g:UltiSnipsSnippetsDir = "$NVIM_SNIPPERS_DIR"
let g:UltiSnipsSnippetsDir="/home/jcas/.config/nvim/snippets"
let g:UltiSnipsEditSplit="vertical"

" markdown-preview
" " set to 1, the nvim will auto close current preview window when change
" " from markdown buffer to another buffer
" " default: 1
let g:mkdp_auto_close = 0

" editorconfig
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"

" YouCompleteMe
let g:ycm_auto_trigger=0

" enable automatic running of :RustFmt when you save a buffer
let g:rustfmt_autosave = 1

" keep cursor column when JK motion
let g:EasyMotion_startofline = 0

""" rainbow
let g:rainbow_active = 1

""" fzf
let $FZF_DEFAULT_COMMAND = 'fd --type f'

""" firenvim
let g:firenvim_config = {
  \ 'globalSettings': {
    \ '<C-w>': 'noop',
    \ '<C-n>': 'default',
  \ }
\ }

""" switch
" disable the default (`gs`) mapping
let g:switch_mapping = ""

" add support for switching snake_case to CamelCase and vice versa
let b:switch_custom_definitions = [
  \   {
  \     '\<[a-z0-9]\+_\k\+\>': {
  \       '_\(.\)': '\U\1'
  \     },
  \     '\<[a-z0-9]\+[A-Z]\k\+\>': {
  \       '\([A-Z]\)': '_\l\1'
  \     },
  \   }
  \ ]

lua << EOF
  require("todo-comments").setup {
    colors = {
      error = { "LspDiagnosticsDefaultError", "ErrorMsg", "#DC2626" },
      warning = { "LspDiagnosticsDefaultWarning", "WarningMsg", "#FBBF24" },
      info = { "LspDiagnosticsDefaultInformation", "#2563EB" },
      hint = { "LspDiagnosticsDefaultHint", "#10B981" },
      default = { "Identifier", "#7C3AED" },
    },
    highlight = {
      before = "", -- "fg" or "bg" or empty
      keyword = "wide", -- "fg", "bg", "wide" or empty. (wide is the same as bg, but will also highlight surrounding characters)
      after = "fg", -- "fg" or "bg" or empty
      pattern = [[.*<(KEYWORDS)\s*:]], -- pattern used for highlightng (vim regex)
      comments_only = true, -- this applies the pattern only inside comments using `commentstring` option
    },
  }

  require'nvim-treesitter.configs'.setup {
    highlight = {
      enable = true,
      custom_captures = {
        -- Highlight the @foo.bar capture group with the "Identifier" highlight group.
        ["foo.bar"] = "Identifier",
      },
    },

    incremental_selection = {
      enable = true,
      keymaps = {
        init_selection = "gnn",
        node_incremental = "grn",
        scope_incremental = "grc",
        node_decremental = "grm",
      },
    },

    indent = {
      enable = true
    }
  }
EOF

if has('termguicolors')
  set termguicolors
endif

""" lightline
let g:lightline = {
      \ 'active': {
      \   'left':  [['mode', 'paste'],
      \             ['readonly', 'python_info', 'filename']],
      \   'right': [['lineinfo'], ['percent'], ['coc_errors', 'coc_warnings', 'fileformat', 'fileencoding', 'filetype']]
      \ },
      \ 'inactive': {
      \   'left':  [['filename']],
      \   'right': [['lineinfo'], ['percent'], ['coc_errors', 'coc_warnings']]
      \ },
      \ 'component_function': {
      \   'filename':     'LightLineFilenameWithModified',
      \   'python_info':  'LightLinePythonInfo',
      \   'coc_errors':   'LightLineCocErrors',
      \   'coc_warnings': 'LightLineCocWarnings'
      \ },
      \ 'colorscheme': 'vim_theme',
      \ }

" if !exists('g:airline_symbols')
"   let g:airline_symbols = {}
" endif

" " let g:airline_theme = 'sonokai'
" let g:airline_theme = 'tokyonight'
" let g:airline#extensions#tabline#enabled = 1
" let g:airline_left_sep = ''
" let g:airline_left_alt_sep = ''
" let g:airline_right_sep = ''
" let g:airline_right_alt_sep = ''
" let g:airline_symbols.branch = ''
" let g:airline_symbols.linenr = '☰'
" let g:airline_symbols.readonly = ''
" let g:airline#parts#ffenc#skip_expected_string='utf-8[unix]'
" let g:airline_section_z = '%{g:airline_symbols.linenr} %l/%l:%v'
