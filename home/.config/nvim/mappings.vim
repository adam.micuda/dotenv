""" REMAP LEADER """

" map , (reverse of ;) to \
noremap \ ,
" use , as <leader>
let mapleader=','


""" NORMAL + VISUAL + OPERATOR PENDING MODES """

" make Y behave consistently with D and C
" noremap Y y$

" toggle visibility of nonprintable characters
map <leader>h :set list!<cr>

" use ,m to run make
map <leader>m :make!<cr>

" use ,n to hide search results
map <leader>n :nohl<cr>

" reindent whole file (make mark, jump to bof, = to eof, back to mark)
" reindent whole file (make mark, jump to bof, = to eof, back to mark)
map <leader>i mmgg=G`m
" map <leader>i mmgg=G`mmm


" use ,f and ,b for fuzzy search in files and buffers
map <leader>f  :Files<cr>
map <leader>p  :Files ..<cr>
map <leader>b  :Buffers<cr>
map <leader>rg :Rg<cr>

" tagbar
map <leader>tb :TagbarToggle<cr>

" NERDTree
map <silent> <C-n> :NERDTreeToggle<CR>
" use ,, to switch btw. alternate files
" this is slow, because easymotion has mapping starting with ,,
map <leader><leader> <c-^>

""" EasyMotion configuration
let g:EasyMotion_do_mapping = 0 " Disable default mappings
let g:EasyMotion_smartcase  = 1 " Turn on case-insensitive feature

" JK motions: Line motions
map <leader>j <Plug>(easymotion-j)
map <leader>k <Plug>(easymotion-k)

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
map  s <Plug>(easymotion-bd-f)
nmap s <Plug>(easymotion-overwin-f)

" `s{char}{char}{label}`
nmap <leader><leader>s <Plug>(easymotion-overwin-f2)

" Move to line
map <leader>L <Plug>(easymotion-bd-jk)
nmap <leader>L <Plug>(easymotion-overwin-line)

" Move to word
map  <leader>w <Plug>(easymotion-bd-w)
nmap <leader>w <Plug>(easymotion-overwin-w)

" close the previous window, especially used for closing floating windows
map <leader>c <c-w>p :q<cr>

" git mappings
" map <leader>gs :Gstatus<bar>resize 20<cr>

""" incsearch
map / <Plug>(incsearch-forward)
map ? <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)

""" incsearch fuzzy
map z/ <Plug>(incsearch-fuzzy-/)
map z? <Plug>(incsearch-fuzzy-?)
map zg/ <Plug>(incsearch-fuzzy-stay)


""" NORMAL MODE """

" zoom a vim pane, <C-w>= to re-balance
nnoremap <leader>- :wincmd _<cr>:wincmd \|<cr>
nnoremap <leader>= :wincmd =<cr>

" disable arrows
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" use enter in normal mode to insert newline
nnoremap <CR> o<Esc>k

" Use K to show documentation in preview window
nnoremap <silent> K :call ShowDocumentation()<CR>

" tabularize around block by second space, useful for python imports
nmap <leader>t gaap2<space>

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" " Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" shortcuts to edit files from current dir
nnoremap <leader>e :e %%
nnoremap <leader>es :sp %%
nnoremap <leader>ev :vsp %%
nnoremap <leader>et :tabe %%

" shortcuts to edit files in splits / tabs
nnoremap <leader>os :sp<space>
nnoremap <leader>ov :vsp<space>
nnoremap <leader>ot :tabe<space>

" switch btw common alternatives, e.g. true/false
nmap <leader>sw :Switch<cr>

" MarkdownPreview
nmap <leader>mp <Plug>MarkdownPreview
" nmap <leader>mx <Plug>MarkdownPreviewStop   FIXME: does not work, puts "top" at cursor position
" nmap <leader>mp <Plug>MarkdownPreviewToggle FIXME: does not close

" YouCompleteMe bindings
nmap <leader>gg :YcmCompleter GoTo<cr>
nmap <silent>gd :YcmCompleter GoToDeclaration<cr>
nmap <silent>gi :YcmCompleter GoToDefinition<cr>
nmap <silent>gr :YcmCompleter GoToReferences<cr>

" CoC bindings
nmap <leader>si :CocCommand python.setInterpreter<cr>
" nmap <silent> gd <Plug>(coc-definition)
" nmap <silent> gy <Plug>(coc-type-definition)
" nmap <silent> gi <Plug>(coc-implementation)
" nmap <silent> gr <Plug>(coc-references)

" Use `[g` and `]g` to navigate diagnostics
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)
nmap <leader>ref <Plug>(coc-refactor)

" Remap for do codeAction of selected region, ex: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap for do codeAction of current line
nmap <leader>ac  <Plug>(coc-codeaction)
" Fix autofix problem of current line
nmap <leader>qf  <Plug>(coc-fix-current)

" hide all float windows
nmap <leader>cf <Plug>(coc-float-hide)

" Use <C-d> for select selections ranges, needs server support, like: coc-tsserver, coc-python
" nmap <silent> <C-d> <Plug>(coc-range-select)
" xmap <silent> <C-d> <Plug>(coc-range-select)

" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>


""" VISUAL + OPERATOR PENDING MODE """

" Create mappings for function text object, requires document symbols feature of languageserver.
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)


""" INSERT MODE """

" use c-d as delete in insert mode
inoremap <c-d> <del>

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ CheckBackSpace() ? "\<TAB>" :
  \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current position.
" Coc only does snippet and additional edit on confirm.
inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" Or use `complete_info` if your vim support it, like:
" inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"


""" COMMAND LINE MODE """

" %% in command expands to current dir
cnoremap %% <C-R>=fnameescape(expand('%:h')).'/'<cr>


""" SUBCOMMANDS """

" Use `:Format` to format current buffer
command! -nargs=0 Format :call CocAction('format')

" Use `:Fold` to fold current buffer
command! -nargs=? Fold :call ('fold', <f-args>)

" use `:OR` for organize import of current buffer
command! -nargs=0 OR :call ('runCommand', 'editor.action.organizeImport')
