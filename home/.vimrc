set rtp+=~/.fzf
set rtp+=~/.local/lib/python3.6/site-packages/powerline/bindings/vim/

" vundle
call plug#begin('~/.vim/bundle')

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --key-bindings --completion --no-update-rc' }
Plug 'junegunn/fzf.vim'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-dispatch'
Plug 'airblade/vim-gitgutter'
Plug 'mileszs/ack.vim'
Plug 'Valloric/YouCompleteMe'
Plug 'scrooloose/nerdtree'
Plug 'editorconfig/editorconfig-vim'
Plug 'benmills/vimux'
Plug 'janko/vim-test'
Plug 'junegunn/vim-easy-align'
Plug 'iamcco/markdown-preview.vim'
Plug 'pgdouyon/vim-evanesco'
Plug 'honza/vim-snippets'

call plug#end()

" use 256 colors
set t_Co=256
if has('gui_running')
  colorscheme desert
endif

" NERDTree
map <silent> <c-n> :NERDTreeToggle<cr>
let NERDTreeIgnore = ['\.bst$','\.dia$','\.eps$','\.pdf$','\~$','\.pyc$','^__pycache__$']

set number
set relativenumber
set backupcopy=yes
set hlsearch
set incsearch

" maintain undo btw sessions
set undodir=~/.vim/undodir
set undofile

" filetype associations
au BufRead,BufNewFile *.txt    set filetype=zim
au BufRead,BufNewFile *.md     set filetype=markdown
au BufRead,BufNewFile *.asd    set filetype=lisp
au BufRead,BufNewFile *.tex    set filetype=tex
au BufRead,BufNewFile *.scheme set filetype=scheme
au BufRead,BufNewFile *.god    set filetype=ruby
au BufRead,BufNewFile *.py     set textwidth=100 tabstop=2 softtabstop=2 shiftwidth=2 foldmethod=indent
au FileType sh                 set noexpandtab

" map , (reverse of ;) to \
noremap \ ,
" use , as <leader>
let mapleader=','

" toggle visibility of nonprintable characters
map <leader>h :set list!<cr>

" visual symbols for nonprintable characters
set listchars=tab:▸\ ,eol:¬

" strip trailing whitespace when saving file
function! <SID>StripTrailingWhitespaces()
  " Preparation: save last search, and cursor position.
  let _s=@/
  let l = line(".")
  let c = col(".")
  " Do the business:
  %s/\s\+$//e
  " Clean up: restore previous search history, and cursor position
  let @/=_s
  call cursor(l, c)
endfunction

autocmd BufWritePre * :call <SID>StripTrailingWhitespaces()

" always show at least 5 lines around cursor
set scrolloff=5
set sidescrolloff=5
set display+=lastline

" use ripgrep as grep program
set grepprg=rg

" set search matches highlighting color
hi Search ctermbg=13

" use c-d as delete in insert mode
inoremap <c-d> <del>

" for compatibility with fish shell
set shell=/bin/bash

" RSpec.vim mappings
" map <leader>t :call RunCurrentSpecFile()<cr>
" map <leader>s :call RunNearestSpec()<cr>
" map <leader>l :call RunLastSpec()<cr>
" map <leader>a :call RunAllSpecs()<cr>

" %% in command expands to current dir
cnoremap %% <c-r>=fnameescape(expand('%:h')).'/'<cr>

" shortcuts to edit files from current dir
map <leader>e :e %%
map <leader>es :sp %%
map <leader>ev :vsp %%
map <leader>et :tabe %%
" shortcuts to edit files in splits / tabs
map <leader>os :sp<space>
map <leader>ov :vsp<space>
map <leader>ot :tabe<space>

" use ,m to run make
map <leader>m :make!<cr>

" needed for % to jump begin/def/class <-> end, etc.
runtime macros/matchit.vim

" use ,f and ,b for fuzzy search in files and buffers
map <leader>f :Files<cr>
map <leader>p :Files ..<cr>
map <leader>b :Buffers<cr>

" GoTo
nmap <leader>gg :YcmCompleter GoTo<cr>
nmap <leader>gc :YcmCompleter GoToDeclaration<cr>
nmap <leader>gf :YcmCompleter GoToDefinition<cr>
nmap <leader>gr :YcmCompleter GoToReferences<cr>

" use enter in normal mode to insert newline
nnoremap <cr> o<esc>k
autocmd CmdwinEnter * nnoremap <cr> <cr>
autocmd BufReadPost quickfix nnoremap <cr> <cr>

" use ,n to hide search results
map <leader>n :nohl<cr>

" use <space><char> to insert single character from normal mode
nmap <space> i_<esc>r

set laststatus=2
set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
set textwidth=100
" see :h fo-table, add a to reformat paragraph after every change
" (:set formatoptions+=a)
set formatoptions=tcrqj
" set formatprg=par\ -w80r

" highlight current line
set cursorline

" use ,, to switch btw. alternate files
" this is slow, because easymotion has mapping starting with ,,
map <leader><leader> <c-^>

set wildignore+=.git*,coverage/**,tmp/**,**~

" code folding options
set foldmethod=syntax
set foldlevel=1
set nofoldenable

" open new splits right and below
set splitbelow
set splitright

" use ctrl + hjkl to move between splits
nnoremap <c-j> <c-w><c-j>
nnoremap <c-k> <c-w><c-k>
nnoremap <c-l> <c-w><c-l>
nnoremap <c-h> <c-w><c-h>

" complete with tab, only to longest match
" imap <tab> <c-p>
set completeopt=menu,longest,preview

" completion in command
set wildmode=longest,list:longest

" reindent whole file (make mark, jump to bof, = to eof, back to mark)
map <leader>i mmgg=G`m

set winwidth=85
set winheight=5
set winminheight=5
set winheight=999

let g:ackprg = 'rg --vimgrep --smartcase'

" vimux
let g:VimuxHeight = "50"
let g:VimuxOrientation = "h"
let g:VimuxPromptString = "> "

map <leader>vp :VimuxPromptCommand<cr>
map <leader>vl :VimuxRunLastCommand<cr>
map <leader>vq :VimuxCloseRunner<cr>
map <leader>vi :VimuxInspectRunner<cr>
map <leader>vz :VimuxZoomRunner<cr>

nnoremap <leader>bd :call VimuxRunCommand("npm run build-dev")<cr>
nnoremap <leader>bt :call VimuxRunCommand("npm run build-test")<cr>
nnoremap <leader>ba :call VimuxRunCommand("npm run build-assets")<cr>
nnoremap <leader>tt :call VimuxRunCommand("npm run test " . expand("%"))<cr>
nnoremap <leader>ta :call VimuxRunCommand("npm run tests")<cr>
nnoremap <leader>tp :call VimuxRunCommand("pyenv activate django3.6 && ./manage.py test")<cr>

" let test#strategy      = 'dispatch'
let test#strategy      = 'vimux'
let test#python#runner = 'pytest'

" mapping test commands
nmap <leader>tn :TestNearest<cr>
nmap <leader>tf :TestFile<cr>
nmap <leader>ts :TestSuite<cr>
nmap <leader>tl :TestLast<cr>
nmap <leader>tg :TestVisit<cr>

" YouCompleteMe
" let g:ycm_python_binary_path = '/home/jakub/.pyenv/versions/3.7.0/bin/python3.7'
let g:ycm_complete_in_comments = 1
let g:ycm_seed_identifiers_with_syntax = 1
let g:ycm_filepath_completion_use_working_dir = 1
let g:ycm_autoclose_preview_window_after_insertion = 1
" let g:ycm_semantic_triggers = { 'elm' : ['.'] }

" disable arrows
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>

" git mappings
" map <leader>gs :Gstatus<bar>resize 20<cr>

" editorconfig
let g:EditorConfig_exclude_patterns = ['fugitive://.*']

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" " Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)
