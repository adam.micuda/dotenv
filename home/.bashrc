# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

function is-interactive-shell {
	[[ $- == *i* ]]
}

export PATH="$HOME/.local/bin:$PATH"

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv init --path)"
eval "$(pyenv virtualenv-init -)"

export PATH="$HOME/.poetry/bin:$PATH"

# Alias definitions.
# Some aliases are used if not running interactively.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi

# If not running interactively, don't do anything
is-interactive-shell || return

# powerline
# FIXME: find or get python path
source $HOME/.local/lib/python3.8/site-packages/powerline/bindings/bash/powerline.sh

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=90000
HISTFILESIZE=30000

# share history between shells
# export PROMPT_COMMAND="history -a;$PROMPT_COMMAND"

function reload-history {
	history -c; history -r
}

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm|xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    if [[ ${EUID} == 0 ]] ; then
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;31m\]\u@\h\[\033[01;34m\] \W \$\[\033[00m\] '
    elif [[ "${USER}" == "jcas" ]] ; then
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;36m\]\u@\h\[\033[00m\] \[\033[01;34m\]\w \$\[\033[00m\] '
    else
        PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\] \[\033[01;34m\]\w \$\[\033[00m\] '
    fi
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h \w \$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h \w\a\]$PS1"
    ;;
*)
    ;;
esac

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    source /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    source /etc/bash_completion
  fi
fi

if [ -x /usr/bin/mint-fortune ]; then
     /usr/bin/mint-fortune
fi

export EDITOR=nvim

# tmuxinator
# TODO: get TMUXINATOR_CONFIG value from cookiecutter template variable
export TMUXINATOR_CONFIG=$HOME/.tmuxinator
source ~/.tmuxinator.bash

# ripgrep
export RIPGREP_CONFIG_PATH=$HOME/.config/ripgrep/rc

# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# fzf
[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# Setting fd as the default source for fzf
export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow --exclude .git'

# To apply the command to CTRL-T as well
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"

# enables use of fzf with prefix notation
function f() { exec $@ | fzf; }

# for each directory executes command
function for-each-dir() {
	for d in $(find . -maxdepth 1 -type d ! -path .); do
		echo $d && (cd $d && exec $@)
 	done
}

function load-dotenv {
	env_file="${1:-.env}"
  env_file_path="$(pwd)/$env_file"
	env_file_exists=$([ ! -f "$env_file_path" ] || echo $?)

  if [ "$env_file_exists" ]; then
    export $(cat "$env_file_path" | sed 's/#.*//g' | xargs)
  fi
}

function pytest-loop {
	config_file="pytest_watch.ini"
	config_exists=$([ ! -f "$(pwd)/$config_file" ] || echo $?)

	pytest="pytest $@"

	if pyenv which ptw &> /dev/null; then
		if [ "$config_exists" ]; then
			python -m pytest_watch --config "$config_file" --runner "$pytest"
		else
			python -m pytest_watch --runner "$pytest"
		fi
	else
		if [ "$config_exists" ]; then
			pytest -c "$config_file" "$@"
		else
			$($pytest)
		fi
	fi
}

function reset-caches {
	caches=`find . -type d -iname "__pycache__" -o -iname "\.pytest_cache" -o -iname "\.testmondata" -o -iname "\.mypy_cache"`

	for d in $caches; do
		rm -r $d;
	done;
}
