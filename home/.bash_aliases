# expand aliases for non-interactive shell
shopt -s expand_aliases

alias g='git'
source /usr/share/bash-completion/completions/git
__git_complete g __git_main

alias vim='nvim'

# projects
alias cd-dotenv='cd ~/projects/dotenv'
alias kredit-backend='cd ~/projects/alto/python/datastore-kredit'
alias kredit='cd ~/projects/alto/js/kredit'
alias canteen-backend='cd ~/projects/alto/python/datastore-canteen'
alias canteen='cd ~/projects/alto/js/canteen'
alias foodie-backend='cd ~/projects/alto/foodie/backend'
alias foodie='cd ~/projects/alto/foodie/frontend'
alias expedice='cd ~/projects/www/expedice-peru/exp'

# some more ls aliases
alias ls='exa'
alias ll='ls --long --classify'
alias la='ls --all --long --classify'

alias diff='diff --color=always -U 1'
alias cat='bat'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias dir='dir --color=auto'
    alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
